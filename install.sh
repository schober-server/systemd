#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

cp docker-main@.service /etc/systemd/system/
cp docker-photoprism@.service /etc/systemd/system/


systemctl daemon-reload
